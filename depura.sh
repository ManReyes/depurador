LIMITE="80"
df -hP > tmp 2>/dev/null
for x in $(cat fs.properties)
do
  FS=$(echo $x | awk '{print $1}' FS=";")
  RUTE=$(echo $x | awk '{print $2}' FS=";")
  FILE=$(echo $x | awk '{print $3}' FS=";")
  
  VALOR=$(grep -w -e "$FS$" tmp | awk '{ print $5}' | awk '{ print $1}' FS="%")
  if [ $VALOR -gt $LIMITE ]
  then
    echo "$FS al $VALOR porciento, limite $LIMITE%"
    LOCAL=$(pwd)
    cd $RUTE
    NARCHIVOS=$(find . -name "*$FILE" 2>/dev/null | wc -l 2>/dev/null)
    if [ $NARCHIVOS -gt 0 ]
    then
      echo "Depurando archivos $FILE en $RUTE"
      echo find . -name "*$FILE"
    else
      echo "No hay archivos $FILE en $RUTE"
    fi
    cd $LOCAL
  else
    echo "$FS al $VALOR porciento"
  fi
done
rm tmp
